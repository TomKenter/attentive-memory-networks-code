# Attentive Memory Networks

This repository holds the code that goes with [Attentive Memory Networks: Efficient Machine Reading for Conversational Search](https://arxiv.org/abs/1712.07229), Tom Kenter and Maarten de Rijke, Proceedings of 1st International Workshop on Conversational Approaches to Information Retrieval, at SIGIR 2017, Tokyo, Japan, August 11, 2017 (CAIR'17).

Feel free to cite the paper ;-). This is the bibtex:

    @inproceedings{kenter2016siamesecbow,
      title={Attentive Memory Networks: Efficient Machine Reading for Conversational Search},
      author={Kenter, Tom and de Rijke, Maarten},
      booktitle={Proceedings of 1st International Workshop on Conversational Approaches to Information Retrieval, at SIGIR 2017 (CAIR'17)},
      year={2017}
    }

## Overview

The Attentive Memory Network reads documents and answers questions about them.
It is a sequence-to-sequence model, with two encoders, one for the question and
one for the documnt.
Also, it contains a memory module -- a separate RNN between encoding and
decoding.

The file `amn.py` holds the main script.
The implementation of the Attentive Memory Network itself is in `amnModel.py`.

Please note that the code is based on the seq2seq code that comes with the
Tensorflow distribution. In particular, `models/tutorials/rnn/translate/translate.py`.

### Dependencies

* Tensorflow version 0.12 for the code in this directory.
* For a Tensorflow 1.6 compatible version, see [the tf_1_6 directory](./tf_1_6/).

## Running the code

In the paper the code is tested on the 20 bAbi QA sets.
The original bAbi files can be downloaded from <https://research.fb.com/downloads/babi/>.
We included feature files for the 20 bAbi sets, which just hold identifiers corresponding to words in a vocabulary.

The following should run from the directory this repository is installed in (provided the directory `myCheckpoints/qa1` exists):

    $ python amn.py --qa_set qa1 --max_steps 10000 --steps_per_checkpoint 200 --output_keep_prob 0.9 --learning_rate 0.1 --size 64 --embedding_size 64 --num_layers 1 --num_memories 3 --train_path bAbi_files/qa1/qa1_train --dev_path bAbi_files/qa1/qa1_dev --checkpoints_save_dir myCheckpoints/qa1 --v

In the checkpoints directory, a new directory will be created based on the hyperparameters (it is done this way to allow for multiple runs, with different hyperparameter settings, to run in parallel).
To test the resulting model, run the following (the model we test happens to be the one stored at the 2400'th step):

    $ python amn.py --test --qa_set qa1 --batch_size 50 --size 64 --embedding_size 64 --num_layers 1 --num_memories 3 --test_path bAbi_files/qa1/qa1_single-supporting-fact_test --checkpoint_file myCheckpoints/qa1/lr_0_1_lrd_0_5_opt_adam_okp_0_9_mgn_1_bs_50_sz_64_esz_64_nl_1_nm_3_nmh_1_svs_24_tvs_24_msl_12_nms_no/checkpoint-2400