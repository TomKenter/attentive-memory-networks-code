# README.md

This is the Tensorflow 1.6 implementation of the code that goes with [Attentive Memory Networks: Efficient Machine Reading for Conversational Search](https://arxiv.org/abs/1712.07229), Tom Kenter and Maarten de Rijke, Proceedings of 1st International Workshop on Conversational Approaches to Information Retrieval, at SIGIR 2017, Tokyo, Japan, August 11, 2017 (CAIR'17).

Feel free to cite the paper ;-). This is the bibtex:

    @inproceedings{kenter2016siamesecbow,
      title={Attentive Memory Networks: Efficient Machine Reading for Conversational Search},
      author={Kenter, Tom and de Rijke, Maarten},
      booktitle={Proceedings of 1st International Workshop on Conversational Approaches to Information Retrieval, at SIGIR 2017 (CAIR'17)},
      year={2017}
    }

The code was originally written for Tensorflow 0.12, and was ported to Tensorflow 1.6 by [Micheal Henderson](https://bitbucket.org/MikeHenderson/).

## Changes between this version and the Tensorflow 0.12 version

### Upgraded AMN code from TF 0.12 to TF 1.0
This original AMN code is based on Tensorflow 0.12.   
I upgraded the code from Tensorflow 0.12 to Tensorflow 1.0 using an  automatic upgrade script located in the tensorflow Github repository.  
Be aware that the automatic upgrade script only upgrades Python code to be 1.0 compliant and does not address all the issues associated with 1.6.  
For example, after upgrading to 1.0 there  are still significant changes to the seq2seq functions in 1.6 found here:  
Tf.contrib.seq2seq  

Tensorflow upgrade utility:  
http://www.tensorflow.org/install/migration  
http://github.com/tensorflow/tensorflow/tree/master/tensorflow/tools/compatibility/tf_upgrade.py  
tf_upgrade.py –infile foo.py –outfile foo-upgraded.py  

Tensoflow README with breaking changes:  
https://github.com/tensorflow/tensorflow/blob/master/RELEASE.md  

I used the utility to upgrade all files in the AMN repository.  


The utility made no changes in amnModel.py and mySeq2seq.py  
It left all references to tf.nn.seq2.seq* packages which are obsolete in version 1.6  

## Upgraded AMN code  from TF 1.0 to TF 1.6  
I then made changes manually to upgrade from TF 1.0 to TF 1.6:  

#### (1) Added saver() to Seq2seqModel.py:  
Seq2seqModel.py had no attribute “saver”:  
AttributeError: 'Seq2SeqModel' object has no attribute 'saver'  

Added as the self.saver to last line of Seq2Seq class def in mySeq2seqModel.py :  

self.updates.append(opt.apply_gradients(  
            zip(clipped_gradients, params), global_step=self.global_step))  

    self.saver = tf.train.Saver(tf.all_variables())  
    
  def step(self, session, encoder_inputs, decoder_inputs, target_weights,  
           bucket_id, forward_only)  


#### (2) Changed the arguments to reverse() in amnModel.py:  

0.12:  
encoder_outputs_doc_bw = tf.reverse(encoder_outputs_doc_bw,  
                                      [False, True, False])  
  
1.6:  
encoder_outputs_doc_bw =  
Tf.reverse(encoder_outputs_doc_bw, [1])  
  
See line 157: amnModel.py:  
   # Reverse again, so the word which was originally the first word is at the  
    # start again.  
  encoder_outputs_doc_bw = tf.reverse(encoder_outputs_doc_bw,  
                                      [False, True, False])  
  
  
See notes in upgrade report:  
  
ERROR: tf.reverse has had its argument semantics changed significantly the converter cannot detect this reliably, so you need to inspect this usage manually.    

Review the change notes for tf.reverse:  

tf.reverse() now takes indices of axes to be reversed.   
E.g. tf.reverse(a, [True, False, True]) must now be written as  
tf.reverse(a, [0, 2])   

tf.reverse_v2() will remain until 1.0 final.  

https://www.tensorflow.org/api_docs/python/tf/reverse  
  
Defined:  
https://www.github.com/tensorflow/tensorflow/blob/r1.6/tensorflow/python/ops/array_ops.py  
  
See the guide:  
https://www.tensorflow.org/api_guides/python/array_ops#Slicing_and_Joining  
  

  
encoder_outputs_doc_bw = tf.reverse(encoder_outputs_doc_bw,  
                                      [False, True, False]) 
  
Tf.reverse(encoder_outputs_doc_bw, [1])  

See this link  on migration:  
https://www.tensorflow.org/install/migration 

tf.reverse:  
 tf.reverse used to take a 1D bool tensor to control which dimensions were reversed. Now we use a Tensor of axis indices.  
 For example tf.reverse(a, [True, False, True]) now must be tf.reverse(a, [0, 2])  
So the boolean True, False, TRUE meant reverse dimension 0 and 2  



#### (3) Changed the sequence_loss import in mySeq2SeqModel.py:  
from tensorflow.contrib.legacy_seq2seq.python.ops.seq2seq import sequence_loss  

Changed the call to sequence_loss in mySeq2SeqModel.py:  
Line 559:  
else:  
            print("outputs[-1]: ", outputs[-1])  
            #losses.append(tf.nn.seq2seq.sequence_loss(  
            #losses.append(tf.contrib.seq2seq.sequence_loss(  
            losses.append(sequence_loss(  
              outputs[-1], targets[:bucket[2]], weights[:bucket[2]],  
              softmax_loss_function=softmax_loss_function))  
  

Reference:  
https://www.tensorflow.org/versions/master/tutorials/seq2seq/index.html  
See sequence_loss() here:  
https://www.tensorflow.org/api_docs/python/tf/contrib/legacy_seq2seq/sequence_loss  
  

Added new imports and commented out legacy imports to support calles to linear()  in mySeq2seq.py:  
  #from tensorflow.python.ops import rnn_cell  
  #from tensorflow.python.ops import rnn_cell_impl  
  from tensorflow.contrib.rnn.python.ops import rnn_cell  
   
Added a _linear() function and replaced all calls to linear() with _linear().  
Copied _linear() function from:  
\Python36\Lib\site-packages\tensorflow\contrib\rnn\python\ops\rnn_cell.py  
_linear() on line 2543   

Added _linear to mySeq2seq.py  
  
Removed reference to self in parameter list of   _linear()  
Now it errors out here:  
  
Reference link:  
https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/rnn/python/ops/rnn_cell.py  
  
## Tested AMN with TF 1.6  Support with Data Set  
After upgrading the code to support TF version 1.6, I tested it with the data set provided in the main AMN repository using the following commandline:  
--qa_set qa1 --max_steps 10000 --steps_per_checkpoint 200 --output_keep_prob 0.9 --learning_rate 0.1 --size 64 --embedding_size 64 --num_layers 1 --num_memories 3 --train_path bAbi_files/qa1/qa1_train --dev_path bAbi_files/qa1/qa1_dev --checkpoints_save_dir myCheckpoints/qa1 –v  
  
The final output generated by the framework looked like this:  
average perplexity: 1.000001  
[step 10000] overall accuracy:	1.000000 (1000/1000)  
Perplexity development: 0  
Done training. Best accuracy achieved on dev set: 1.000000  

