# Copyright 2015 Tom Kenter. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# This file is an augmented version of 
# tensorflow/models/rnn/translate/data_utils.py 

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import re

from tensorflow.python.platform import gfile

# Special vocabulary symbols - we always put them at the start.
_PAD = b"_PAD"
_GO = b"_GO"
_EOS = b"_EOS"
_UNK = b"_UNK"
_START_VOCAB = [_PAD, _GO, _EOS, _UNK]

PAD_ID = 0
GO_ID = 1
EOS_ID = 2
UNK_ID = 3

# Regular expressions used to tokenize.
_WORD_SPLIT = re.compile(b"([.,!?\"':;)(])")
_DIGIT_RE = re.compile(br"\d")


def basic_tokenizer(sentence):
  """Very basic tokenizer: split the sentence into a list of tokens."""
  words = []
  for space_separated_fragment in sentence.strip().split():
    words.extend(_WORD_SPLIT.split(space_separated_fragment))
  return [w for w in words if w]


def create_vocabulary(vocabulary_path, data_path, max_vocabulary_size,
                      tokenizer=None, normalize_digits=True):
  """Create vocabulary file (if it does not exist yet) from data file.

  Data file is assumed to contain one sentence per line. Each sentence is
  tokenized and digits are normalized (if normalize_digits is set).
  Vocabulary contains the most-frequent tokens up to max_vocabulary_size.
  We write it to vocabulary_path in a one-token-per-line format, so that later
  token in the first line gets id=0, second line gets id=1, and so on.

  Args:
    vocabulary_path: path where the vocabulary will be created.
    data_path: data file that will be used to create vocabulary.
    max_vocabulary_size: limit on the size of the created vocabulary.
    tokenizer: a function to use to tokenize each data sentence;
      if None, basic_tokenizer will be used.
    normalize_digits: Boolean; if true, all digits are replaced by 0s.
  """
  if not gfile.Exists(vocabulary_path):
    print("Creating vocabulary %s from data %s" % (vocabulary_path, data_path))
    vocab = {}
    with gfile.GFile(data_path, mode="rb") as f:
      counter = 0
      for line in f:
        counter += 1
        if counter % 100000 == 0:
          print("  processing line %d" % counter)
        tokens = tokenizer(line) if tokenizer else basic_tokenizer(line)
        for w in tokens:
          word = _DIGIT_RE.sub(b"0", w) if normalize_digits else w
          if word in vocab:
            vocab[word] += 1
          else:
            vocab[word] = 1
      vocab_list = _START_VOCAB + sorted(vocab, key=vocab.get, reverse=True)
      if len(vocab_list) > max_vocabulary_size:
        vocab_list = vocab_list[:max_vocabulary_size]
      with gfile.GFile(vocabulary_path, mode="wb") as vocab_file:
        for w in vocab_list:
          vocab_file.write(w + b"\n")

def buildVocab(data_path, vocab, tokenizer=None, normalize_digits=True):
  counter = 0
  print ("build_vocab: ", data_path)
  with gfile.GFile(data_path, mode="rb") as f:
    for line in f:
      counter += 1
      if counter % 100000 == 0:
        print("  %s -- processing line %d" % (data_path, counter))
      tokens = tokenizer(line) if tokenizer else basic_tokenizer(line)
      for w in tokens:
        if w in _START_VOCAB: # Ignore special tokens
          continue
        word = _DIGIT_RE.sub(b"0", w) if normalize_digits else w
        if word in vocab:
          vocab[word] += 1
        else:
          vocab[word] = 1

def create_vocabulary_from_two_files(vocabulary_path,
                                     data_path_1, data_path_2,
                                     max_vocabulary_size,
                                     tokenizer=None, normalize_digits=True):
  print("data_path_1 full path (following symlinks)")
  full_path = os.path.realpath(data_path_1)
  print(full_path + "\n")

  if not gfile.Exists(vocabulary_path):
    print("Creating vocabulary %s from data %s and %s" % (
      vocabulary_path, data_path_1, data_path_2))
    vocab = {}

    buildVocab(data_path_1, vocab, tokenizer=tokenizer,
               normalize_digits=normalize_digits)
    buildVocab(data_path_2, vocab, tokenizer=tokenizer,
               normalize_digits=normalize_digits)

    vocab_list = _START_VOCAB + sorted(vocab, key=vocab.get, reverse=True)
    if len(vocab_list) > max_vocabulary_size:
      vocab_list = vocab_list[:max_vocabulary_size]

    with gfile.GFile(vocabulary_path, mode="wb") as vocab_file:
      for w in vocab_list:
        vocab_file.write(w + b"\n")

def initialize_vocabulary(vocabulary_path):
  """Initialize vocabulary from file.

  We assume the vocabulary is stored one-item-per-line, so a file:
    dog
    cat
  will result in a vocabulary {"dog": 0, "cat": 1}, and this function will
  also return the reversed-vocabulary ["dog", "cat"].

  Args:
    vocabulary_path: path to the file containing the vocabulary.

  Returns:
    a pair: the vocabulary (a dictionary mapping string to integers), and
    the reversed vocabulary (a list, which reverses the vocabulary mapping).

  Raises:
    ValueError: if the provided vocabulary_path does not exist.
  """
  if gfile.Exists(vocabulary_path):
    rev_vocab = []
    with gfile.GFile(vocabulary_path, mode="rb") as f:
      rev_vocab.extend(f.readlines())
    rev_vocab = [line.strip() for line in rev_vocab]
    vocab = dict([(x, y) for (y, x) in enumerate(rev_vocab)])
    return vocab, rev_vocab
  else:
    raise ValueError("Vocabulary file %s not found.", vocabulary_path)


def sentence_to_token_ids(sentence, vocabulary,
                          tokenizer=None, normalize_digits=True):
  """Convert a string to list of integers representing token-ids.

  For example, a sentence "I have a dog" may become tokenized into
  ["I", "have", "a", "dog"] and with vocabulary {"I": 1, "have": 2,
  "a": 4, "dog": 7"} this function will return [1, 2, 4, 7].

  Args:
    sentence: the sentence in bytes format to convert to token-ids.
    vocabulary: a dictionary mapping tokens to integers.
    tokenizer: a function to use to tokenize each sentence;
      if None, basic_tokenizer will be used.
    normalize_digits: Boolean; if true, all digits are replaced by 0s.

  Returns:
    a list of integers, the token-ids for the sentence.
  """

  if tokenizer:
    words = tokenizer(sentence)
  else:
    words = basic_tokenizer(sentence)

  if not normalize_digits:
    aIds = [vocabulary.get(w, UNK_ID) for w in words]
    if UNK_ID in aIds:
      print("[ERROR] don't know word '%s'" % words[aIds.index(UNK_ID)])
      exit(1)
    return aIds

  # Normalize digits by 0 before looking words up in the vocabulary.
  return [vocabulary.get(_DIGIT_RE.sub(b"0", w), UNK_ID) for w in words]


def data_to_token_ids(data_path, save_path, vocabulary_path,
                      tokenizer=None, normalize_digits=True):
  """Tokenize data file and turn into token-ids using given vocabulary file.

  This function loads data line-by-line from data_path, calls the above
  sentence_to_token_ids, and saves the result to save_path. See comment
  for sentence_to_token_ids on the details of token-ids format.

  Args:
    data_path: path to the data file in one-sentence-per-line format.
    save_path: path where the file with token-ids will be created.
    vocabulary_path: path to the vocabulary file.
    tokenizer: a function to use to tokenize each sentence;
      if None, basic_tokenizer will be used.
    normalize_digits: Boolean; if true, all digits are replaced by 0s.
  """
  if not gfile.Exists(save_path):
    print("Tokenizing data in %s" % data_path)
    vocab, _ = initialize_vocabulary(vocabulary_path)
    with gfile.GFile(data_path, mode="rb") as data_file:
      with gfile.GFile(save_path, mode="w") as tokens_file:
        counter = 0
        for line in data_file:
          counter += 1
          if counter % 100000 == 0:
            print("  tokenizing line %d" % counter)

          aTokenizedParts = []
          aParts = line.split("\t")
          for sPart in aParts:
            token_ids = sentence_to_token_ids(
              sPart, vocab, tokenizer=tokenizer,
              normalize_digits=normalize_digits)
            aTokenizedParts.append(" ".join([str(tok) for tok in token_ids]))
            
          tokens_file.write("\t".join(aTokenizedParts) + "\n")


def prepare_data(train_path, dev_path, source_vocabulary_size,
                 target_vocabulary_size, tokenizer=None):
  """Get WMT data into data_dir, create vocabularies and tokenize data.

  Args:
    train_path: path prefix  to training files 
    dev_path: path prefix of dev files
    en_vocabulary_size: size of the English vocabulary to create and use.
    fr_vocabulary_size: size of the French vocabulary to create and use.
    tokenizer: a function to use to tokenize each data sentence;
      if None, basic_tokenizer will be used.

  Returns:
    A tuple of 6 elements:
      (1) path to the token-ids for English training data-set,
      (2) path to the token-ids for French training data-set,
      (3) path to the token-ids for English development data-set,
      (4) path to the token-ids for French development data-set,
      (5) path to the English vocabulary file,
      (6) path to the French vocabulary file.
  """
  # Create vocabularies of the appropriate sizes.
  
  source_train_ids_path_questions, source_train_ids_path_docs, \
    source_train_ids_path_qdocs, target_train_ids_path, source_vocab_path, \
    target_vocab_path = prepare_data_tied(train_path, source_vocabulary_size,
                                          target_vocabulary_size,
                                          tokenizer=tokenizer)

  source_dev_ids_path_questions, source_dev_ids_path_docs, \
    source_dev_ids_path_qdocs, target_dev_ids_path, source_vocab_path, \
    target_vocab_path = prepare_data_tied(dev_path, source_vocabulary_size,
                                          target_vocabulary_size,
                                          tokenizer=tokenizer)

  return (source_train_ids_path_questions, source_train_ids_path_docs,
          source_train_ids_path_qdocs, target_train_ids_path,
          source_dev_ids_path_questions, source_dev_ids_path_docs,
          source_dev_ids_path_qdocs, target_dev_ids_path,
          source_vocab_path, target_vocab_path)


def prepare_data_tied(data_path_prefix, source_vocabulary_size,
                      target_vocabulary_size, tokenizer=None):
  """
  In this case we are embedding all source and target files to the same space.
  In other words, there is only one dictionary.
  """
  assert(source_vocabulary_size == target_vocabulary_size), \
    "[ERROR] source and target vocab sizes (%d and %d) should match" % (
      (source_vocabulary_size, target_vocabulary_size))

  data_directory = os.path.dirname(data_path_prefix)

  vocab_path = os.path.join(data_directory,
                            "vocab%d.txt" % source_vocabulary_size)

  # Always create vocabulary of the full questions + docs vocabulary
  create_vocabulary_from_two_files(vocab_path,
                                   "%s_qdocs.source" % data_path_prefix,
                                   "%s.target" % data_path_prefix,
                                   source_vocabulary_size,
                                   tokenizer=tokenizer)

  # Create token ids for the training data.
  source_ids_path_questions = data_path_prefix + \
    ("_questions.ids%d.source" % source_vocabulary_size)
  source_ids_path_docs = data_path_prefix + \
                    ("_docs.ids%d.source" % source_vocabulary_size)
  source_ids_path_qdocs = data_path_prefix + \
                    ("_qdocs.ids%d.source" % source_vocabulary_size)
  target_ids_path = data_path_prefix + \
                    (".ids%d.target" % target_vocabulary_size)

  data_to_token_ids(data_path_prefix + "_questions.source",
                    source_ids_path_questions, vocab_path,
                    tokenizer=tokenizer, normalize_digits=False)
  data_to_token_ids(data_path_prefix + "_docs.source",
                    source_ids_path_docs, vocab_path,
                    tokenizer=tokenizer, normalize_digits=False)
  data_to_token_ids(data_path_prefix + "_qdocs.source",
                    source_ids_path_qdocs, vocab_path,
                    tokenizer=tokenizer, normalize_digits=False)
  data_to_token_ids(data_path_prefix + ".target",
                    target_ids_path, vocab_path,
                    tokenizer=tokenizer, normalize_digits=False)

  return (source_ids_path_questions, source_ids_path_docs,
          source_ids_path_qdocs, target_ids_path,
          vocab_path, vocab_path)

