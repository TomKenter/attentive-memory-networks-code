# Copyright 2015 Tom Kenter. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# This file is an augmented version of:
# tensorflow/models/rnn/translate/seq2seq_model.py.
# The main difference is that amnModel.amn() is used as seq2seq function
# (seq2seq_f).
# Also the encoder inputs come in two (one for question encoder en one for
# document encoder).

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import random

import numpy as np
from six.moves import xrange
import tensorflow as tf

from tensorflow.python.framework import ops
from tensorflow.python.ops import variable_scope

from tensorflow.contrib.legacy_seq2seq.python.ops.seq2seq import sequence_loss


import myDataUtils as data_utils
import amnModel

class Seq2SeqModel(object):
  """Sequence-to-sequence model with attention and for multiple buckets.
  """

  def __init__(self,
               source_vocab_size,
               target_vocab_size,
               sentence_length,
               buckets,
               size,
               embedding_size,
               num_layers,
               num_memories,
               num_heads,
               max_gradient_norm,
               output_keep_prob,
               batch_size,
               learning_rate,
               learning_rate_decay_factor,
               optimizer,
               use_lstm=False,
               num_samples=512,
               forward_only=False,
               dtype=tf.float32,
               debug=False):
    """Create the model.

    Args:
      source_vocab_size: size of the source vocabulary.
      target_vocab_size: size of the target vocabulary.
      buckets: a list of pairs (I, O), where I specifies maximum input length
        that will be processed in that bucket, and O specifies maximum output
        length. Training instances that have inputs longer than I or outputs
        longer than O will be pushed to the next bucket and padded accordingly.
        We assume that the list is sorted, e.g., [(2, 4), (8, 16)].
      size: number of units in each layer of the model.
      num_layers: number of layers in the model.
      max_gradient_norm: gradients will be clipped to maximally this norm.
      batch_size: the size of the batches used during training;
        the model construction is independent of batch_size, so it can be
        changed after initialization if this is convenient, e.g., for decoding.
      learning_rate: learning rate to start with.
      learning_rate_decay_factor: decay learning rate by this much when needed.
      use_lstm: if true, we use LSTM cells instead of GRU cells.
      num_samples: number of samples for sampled softmax.
      forward_only: if set, we do not construct the backward pass in the model.
      dtype: the data type to use to store internal variables.
    """
    self.source_vocab_size = source_vocab_size
    self.target_vocab_size = target_vocab_size
    self.buckets = buckets
    self.batch_size = batch_size
    self.learning_rate = tf.Variable(
        float(learning_rate), trainable=False, dtype=dtype)
    self.learning_rate_decay_op = self.learning_rate.assign(
        self.learning_rate * learning_rate_decay_factor)
    self.output_keep_prob = output_keep_prob
    self.global_step = tf.Variable(0, trainable=False)
    self.num_memories = num_memories
    self.num_heads = num_heads
    self.sentence_length = sentence_length
    self.bDebug = debug
    self.bSampledSoftmax = False
    self.forward_only = forward_only

    # If we use sampled softmax, we need an output projection.
    output_projection = None
    softmax_loss_function = None
    # Sampled softmax only makes sense if we sample less than vocabulary size.
    if num_samples > 0 and num_samples < self.target_vocab_size:
      self.bSampledSoftmax = True
      w_t = tf.get_variable("proj_w", [self.target_vocab_size, size],
                            dtype=dtype)
      self.w = tf.transpose(w_t)
      self.b = tf.get_variable("proj_b", [self.target_vocab_size], dtype=dtype)
      output_projection = (self.w, self.b)

      def sampled_loss(inputs, labels):
        labels = tf.reshape(labels, [-1, 1])
        # We need to compute the sampled_softmax_loss using 32bit floats to
        # avoid numerical instabilities.
        local_w_t = tf.cast(w_t, tf.float32)
        local_b = tf.cast(self.b, tf.float32)
        local_inputs = tf.cast(inputs, tf.float32)
        return tf.cast(
          tf.nn.sampled_softmax_loss(local_w_t, local_b, local_inputs, labels,
                                     num_samples, self.target_vocab_size),
          dtype)
      softmax_loss_function = sampled_loss

    # Create the internal multi-layer cell for our RNN.
    single_cell = tf.nn.rnn_cell.GRUCell(size)
    if use_lstm:
      single_cell = tf.nn.rnn_cell.BasicLSTMCell(size)

    self.ph_output_keep_prob = tf.placeholder(tf.float32,
                                              name='ph_output_keep_prob')
    
    if output_keep_prob < 1.0:
      single_cell = tf.nn.rnn_cell.DropoutWrapper(
        single_cell, output_keep_prob=self.ph_output_keep_prob)

    cell = single_cell
    if num_layers > 1:
      cell = tf.nn.rnn_cell.MultiRNNCell([single_cell] * num_layers)

    # The seq2seq function: we use embedding for the input and attention.
    def seq2seq_f(encoder_inputs, decoder_inputs, do_decode):
      return amnModel.amn(
        encoder_inputs,
        decoder_inputs,
        cell,
        source_vocab_size,
        target_vocab_size,
        sentence_length,
        num_memories,
        embedding_size,
        num_heads=num_heads,
        output_projection=output_projection,
        feed_previous=do_decode,
        dtype=dtype)          

    # Feeds for inputs.
    self.encoder_inputs = [] if self.num_memories == 0 else [[],[]]
    self.decoder_inputs = []
    self.target_weights = []
    if self.num_memories == 0:
      for i in xrange(buckets[-1][0]):  # Last bucket is the biggest one.
        self.encoder_inputs.append(tf.placeholder(tf.int32, shape=[None],
                                                  name="encoder{0}".format(i)))
      for i in xrange(buckets[-1][1] + 1):
        self.decoder_inputs.append(tf.placeholder(tf.int32, shape=[None],
                                                  name="decoder{0}".format(i)))
        self.target_weights.append(tf.placeholder(dtype, shape=[None],
                                                  name="weight{0}".format(i)))
    else:
      for i in xrange(buckets[-1][0]):  # Last bucket is the biggest one.
        self.encoder_inputs[0].append(
          tf.placeholder(tf.int32, shape=[None],
                         name="question_encoder{0}".format(i)))
      # NOTE the sentence length, in words, is fixed.
      for i in xrange(buckets[-1][1] * sentence_length): # Last bucket= biggest
        self.encoder_inputs[1].append(
          tf.placeholder(tf.int32, shape=[None],
                         name="doc_encoder{0}".format(i)))
      for i in xrange(buckets[-1][2] + 1):
        self.decoder_inputs.append(tf.placeholder(tf.int32, shape=[None],
                                                  name="decoder{0}".format(i)))
        self.target_weights.append(tf.placeholder(dtype, shape=[None],
                                                  name="weight{0}".format(i)))

    # Our targets are decoder inputs shifted by one.
    targets = [self.decoder_inputs[i + 1]
               for i in xrange(len(self.decoder_inputs) - 1)]

    # Training outputs and losses.
    if forward_only:
      #self.outputs, self.losses = tf.nn.seq2seq.model_with_buckets(
      self.outputs, self.losses, self.softmaxes = my_model_with_buckets(
        self.encoder_inputs, self.decoder_inputs, targets,
        self.target_weights, sentence_length, buckets,
        lambda x, y: seq2seq_f(x, y, True),
        softmax_loss_function=softmax_loss_function)

      # If we use output projection, we need to project outputs for decoding.
      if output_projection is not None:
        for b in xrange(len(buckets)):
          self.outputs[b] = [
            tf.matmul(output, output_projection[0]) + output_projection[1]
            for output in self.outputs[b]
          ]
    else: # Not forward_only
      self.outputs, self.losses, self.softmaxes = my_model_with_buckets(
        self.encoder_inputs, self.decoder_inputs, targets,
        self.target_weights, sentence_length, buckets,
        lambda x, y: seq2seq_f(x, y, False),
        softmax_loss_function=softmax_loss_function)
  
    # Gradients and SGD update operation for training the model.
    params = tf.trainable_variables()
    if not forward_only:
      self.gradient_norms = []
      self.updates = []

      if optimizer == "sgd":
        opt = tf.train.GradientDescentOptimizer(self.learning_rate)
      else:
        opt = tf.train.AdamOptimizer(self.learning_rate, epsilon=0.1)

      for b in xrange(len(buckets)):
        gradients = tf.gradients(self.losses[b], params)
        clipped_gradients, norm = tf.clip_by_global_norm(gradients,
                                                         max_gradient_norm)
        self.gradient_norms.append(norm)
        self.updates.append(opt.apply_gradients(
            zip(clipped_gradients, params), global_step=self.global_step))

    self.saver = tf.train.Saver(tf.global_variables())
    
  def step(self, session, encoder_inputs, decoder_inputs, target_weights,
           bucket_id, forward_only):
    """Run a step of the model feeding the given inputs.

    Args:
      session: tensorflow session to use.
      encoder_inputs: list of numpy int vectors to feed as encoder inputs.
      decoder_inputs: list of numpy int vectors to feed as decoder inputs.
      target_weights: list of numpy float vectors to feed as target weights.
      bucket_id: which bucket of the model to use.
      forward_only: whether to do the backward step or only forward.

    Returns:
      A triple consisting of gradient norm (or None if we did not do backward),
      average perplexity, and the outputs.

    Raises:
      ValueError: if length of encoder_inputs, decoder_inputs, or
        target_weights disagrees with bucket size for the specified bucket_id.
    """
    # Check if the sizes match.
    if self.num_memories == 0:
      encoder_size, decoder_size = self.buckets[bucket_id]
    else:
      question_encoder_size, doc_encoder_size, decoder_size = \
        self.buckets[bucket_id]

    if self.num_memories == 0:
      if len(encoder_inputs) != encoder_size:
        raise ValueError("Encoder length must be equal to the one in bucket,"
                         " %d != %d." % (len(encoder_inputs), encoder_size))
    else:
      if len(encoder_inputs[0]) != question_encoder_size:
        raise ValueError(
          "Question encoder length must be equal to the one in bucket,"
          " %d != %d." % (len(question_encoder_inputs), question_encoder_size))

      if len(encoder_inputs[1]) != (doc_encoder_size * self.sentence_length):
        raise ValueError(
          "Document encoder length must be equal to the one in bucket,"
          " %d != %d." % (len(encoder_inputs[1]),
                          doc_encoder_size * self.sentence_length))

    if len(decoder_inputs) != decoder_size:
      raise ValueError("Decoder length must be equal to the one in bucket,"
                       " %d != %d." % (len(decoder_inputs), decoder_size))
    if len(target_weights) != decoder_size:
      raise ValueError("Weights length must be equal to the one in bucket,"
                       " %d != %d." % (len(target_weights), decoder_size))

    # Input feed: encoder inputs, decoder inputs, target_weights, as provided.
    input_feed = {}
    if self.num_memories == 0:
      for l in xrange(encoder_size):
        input_feed[self.encoder_inputs[l].name] = encoder_inputs[l]
    else:
      for l in xrange(question_encoder_size):
        input_feed[self.encoder_inputs[0][l].name] = encoder_inputs[0][l]
      # NOTE: sentence length, in words, is ALWAYS 12
      for l in xrange(doc_encoder_size * self.sentence_length):
        input_feed[self.encoder_inputs[1][l].name] = encoder_inputs[1][l]

    for l in xrange(decoder_size):
      input_feed[self.decoder_inputs[l].name] = decoder_inputs[l]
      input_feed[self.target_weights[l].name] = target_weights[l]

    # Since our targets are decoder inputs shifted by one, we need one more.
    last_target = self.decoder_inputs[decoder_size].name
    input_feed[last_target] = np.zeros([self.batch_size], dtype=np.int32)

    # Output feed: depends on whether we do a backward step or not.
    if forward_only:
      output_feed = [self.losses[bucket_id]]  # Loss for this batch.
      for l in xrange(decoder_size):  # Output logits.
        output_feed.append(self.outputs[bucket_id][l])
      output_feed.append(self.softmaxes[bucket_id])
    else:
      output_feed = [self.updates[bucket_id],  # Update Op that does SGD.
                     self.gradient_norms[bucket_id],  # Gradient norm.
                     self.losses[bucket_id]]  # Loss for this batch.

    if forward_only: # No dropout in forward mode
      input_feed[self.ph_output_keep_prob.name] = 1.0
    else:
      input_feed[self.ph_output_keep_prob.name] = self.output_keep_prob

    outputs = session.run(output_feed, input_feed)

    if forward_only:
      # No gradient norm, loss, outputs, softmaxes
      return None, outputs[0], outputs[1:-1], outputs[-1]
    else:
      return outputs[1], outputs[2], None, None  # Gradient norm, loss, no outputs.


  def get_batch(self, data, bucket_id, mode='random', iStartAt=0):
    """Get a random batch of data from the specified bucket, prepare for step.

    To feed data in step(..) it must be a list of batch-major vectors, while
    data here contains single length-major cases. So the main logic of this
    function is to re-index data cases to be in the proper format for feeding.

    Args:
      data: a tuple of size len(self.buckets) in which each element contains
        lists of pairs of input and output data that we use to create a batch.
      bucket_id: integer, which bucket to get the batch for.
      mode: either 'random' or 'deterministic'

    Returns:
      The triple (encoder_inputs, decoder_inputs, target_weights) for
      the constructed batch that has the proper format to call step(...) later.
    """

    if self.num_memories == 0:
      encoder_size, decoder_size = self.buckets[bucket_id]
      encoder_inputs, decoder_inputs = [], []
    else:
      question_encoder_size, doc_encoder_size, decoder_size = \
        self.buckets[bucket_id]
      encoder_inputs, decoder_inputs, candidate_ids = [[], []], [], []

    # Get a random batch of encoder and decoder inputs from data,
    # pad them if needed, reverse encoder inputs and add GO to decoder.
    for i in xrange(iStartAt, iStartAt + self.batch_size):
      if self.num_memories == 0:
        encoder_input, decoder_input = random.choice(data[bucket_id])
  
        # Encoder inputs are padded and then reversed.
        encoder_pad = [data_utils.PAD_ID] * (encoder_size - len(encoder_input))
        encoder_inputs.append(list(reversed(encoder_input + encoder_pad)))
      else:
        if mode == 'random':
          question_encoder_input, doc_encoder_input, decoder_input, \
            candidate_id_list = random.choice(data[bucket_id])
        else:
          question_encoder_input, doc_encoder_input, decoder_input, \
            candidate_id_list = data[bucket_id][i]

        assert (question_encoder_size == len(question_encoder_input)), \
          "[ERROR] wrong question input size: %d, should be %d" % (
            len(question_encoder_input), question_encoder_size)
        assert ( (doc_encoder_size * self.sentence_length) == \
                 len(doc_encoder_input)), \
          "[ERROR] wrong doc input size: %d, should be %d" % (
            len(doc_encoder_input), (doc_encoder_size * self.sentence_length))

        # NOTE: nothing is reversed here..!
        # (Because we want to keep the order of the sentences/questions and
        # padding as is)
        encoder_inputs[0].append(question_encoder_input)
        encoder_inputs[1].append(doc_encoder_input)

      # Decoder inputs get an extra "GO" symbol, and are padded then.
      decoder_pad_size = decoder_size - len(decoder_input) - 1
      decoder_inputs.append([data_utils.GO_ID] + decoder_input +
                            [data_utils.PAD_ID] * decoder_pad_size)

      if candidate_id_list is not None:
        candidate_ids.append(candidate_id_list)

    # Now we create batch-major vectors from the data selected above.
    if self.num_memories == 0:
      batch_encoder_inputs, batch_decoder_inputs, batch_weights = [], [], []

      # Batch encoder inputs are just re-indexed encoder_inputs.
      for length_idx in xrange(encoder_size):
        batch_encoder_inputs.append(
          np.array([encoder_inputs[batch_idx][length_idx]
                    for batch_idx in xrange(self.batch_size)], dtype=np.int32))
    else:
      batch_encoder_inputs, batch_decoder_inputs, batch_weights = \
        [[], []], [], []

      # Batch encoder inputs are just re-indexed encoder_inputs.
      for length_idx in xrange(question_encoder_size):
        batch_encoder_inputs[0].append(
          np.array([encoder_inputs[0][batch_idx][length_idx]
                    for batch_idx in xrange(self.batch_size)], dtype=np.int32))

      # NOTE: sentence length, in words, is ALWAYS 12
      for length_idx in xrange(doc_encoder_size * self.sentence_length):
        batch_encoder_inputs[1].append(
          np.array([encoder_inputs[1][batch_idx][length_idx]
                    for batch_idx in xrange(self.batch_size)], dtype=np.int32))

    # Batch decoder inputs are re-indexed decoder_inputs, we create weights.
    for length_idx in xrange(decoder_size):
      batch_decoder_inputs.append(
          np.array([decoder_inputs[batch_idx][length_idx]
                    for batch_idx in xrange(self.batch_size)], dtype=np.int32))

      # Create target_weights to be 0 for targets that are padding.
      batch_weight = np.ones(self.batch_size, dtype=np.float32)
      for batch_idx in xrange(self.batch_size):
        # We set weight to 0 if the corresponding target is a PAD symbol.
        # The corresponding target is decoder_input shifted by 1 forward.
        if length_idx < decoder_size - 1:
          target = decoder_inputs[batch_idx][length_idx + 1]
        if length_idx == decoder_size - 1 or target == data_utils.PAD_ID:
          batch_weight[batch_idx] = 0.0
      batch_weights.append(batch_weight)

    # NOTE that e.g. batch_decoder_inputs is [decoder_size * 50], while
    # candidate_ids = [50 * nr_of_candidates]
    if len(candidate_ids) == 0:
      candidate_ids = None
    return (batch_encoder_inputs, batch_decoder_inputs, batch_weights, 
            candidate_ids)


  def write(self):
    for v in tf.global_variables():
      print("%s (%s %s): %s" % (v.name, v.device, v.dtype, v.get_shape()))


# Same as tf.nn.seq2seq.model_with_buckets, except we return the attention
# vectors too.
def my_model_with_buckets(encoder_inputs, decoder_inputs, targets, weights,
                          sentence_length, buckets, seq2seq,
                          softmax_loss_function=None, per_example_loss=False,
                          name=None):
  """Create a sequence-to-sequence model with support for bucketing.

  The seq2seq argument is a function that defines a sequence-to-sequence model,
  e.g., seq2seq = lambda x, y: basic_rnn_seq2seq(x, y, rnn_cell.GRUCell(24))

  Args:
    encoder_inputs: A list of Tensors to feed the encoder; first seq2seq input.
    decoder_inputs: A list of Tensors to feed the decoder; second seq2seq input.
    targets: A list of 1D batch-sized int32 Tensors (desired output sequence).
    weights: List of 1D batch-sized float-Tensors to weight the targets.
    buckets: A list of pairs of (input size, output size) for each bucket.
    seq2seq: A sequence-to-sequence model function; it takes 2 input that
      agree with encoder_inputs and decoder_inputs, and returns a pair
      consisting of outputs and states (as, e.g., basic_rnn_seq2seq).
    softmax_loss_function: Function (inputs-batch, labels-batch) -> loss-batch
      to be used instead of the standard softmax (the default if this is None).
    per_example_loss: Boolean. If set, the returned loss will be a batch-sized
      tensor of losses for each sequence in the batch. If unset, it will be
      a scalar with the averaged loss from all examples.
    name: Optional name for this operation, defaults to "model_with_buckets".

  Returns:
    A tuple of the form (outputs, losses), where:
      outputs: The outputs for each bucket. Its j'th element consists of a list
        of 2D Tensors. The shape of output tensors can be either
        [batch_size x output_size] or [batch_size x num_decoder_symbols]
        depending on the seq2seq model used.
      losses: List of scalar Tensors, representing losses for each bucket, or,
        if per_example_loss is set, a list of 1D batch-sized float Tensors.

  Raises:
    ValueError: If length of encoder_inputs, targets, or weights is smaller
      than the largest (last) bucket.
  """
  if type(encoder_inputs[0]) == type(list()):
    if len(encoder_inputs[0]) < buckets[-1][0]:
      raise ValueError(
        "Length of encoder_inputs (%d) must be at least that of la"
        "st bucket (%d)." % (len(encoder_inputs[0]), buckets[-1][0]))
    if len(encoder_inputs[1]) < buckets[-1][1]:
      raise ValueError(
        "Length of encoder_inputs (%d) must be at least that of la"
        "st bucket (%d)." % (len(encoder_inputs[1]), buckets[-1][1]))
    if len(targets) < buckets[-1][2]:
      raise ValueError("Length of targets (%d) must be at least that of last"
                         "bucket (%d)." % (len(targets), buckets[-1][2]))
    if len(weights) < buckets[-1][2]:
      raise ValueError("Length of weights (%d) must be at least that of last"
                       "bucket (%d)." % (len(weights), buckets[-1][2]))
  else:
    if len(encoder_inputs) < buckets[-1][0]:
      raise ValueError(
        "Length of encoder_inputs (%d) must be at least that of la"
        "st bucket (%d)." % (len(encoder_inputs), buckets[-1][0]))
    if len(targets) < buckets[-1][1]:
      raise ValueError("Length of targets (%d) must be at least that of last"
                       "bucket (%d)." % (len(targets), buckets[-1][1]))
    if len(weights) < buckets[-1][1]:
      raise ValueError("Length of weights (%d) must be at least that of last"
                       "bucket (%d)." % (len(weights), buckets[-1][1]))

  all_inputs = encoder_inputs + decoder_inputs + targets + weights
  losses = []
  outputs = []
  all_softmaxes = []
  with ops.name_scope(name, "my_model_with_buckets", all_inputs):
    for j, bucket in enumerate(buckets):
      with variable_scope.variable_scope(variable_scope.get_variable_scope(),
                                         reuse=True if j > 0 else None):
        if type(encoder_inputs[0]) != type(list()):
          aSoftmaxes, (bucket_outputs, _) = seq2seq(encoder_inputs[:bucket[0]],
                                                    decoder_inputs[:bucket[1]])
          outputs.append(bucket_outputs)
          if per_example_loss:
            losses.append(tf.nn.seq2seq.sequence_loss_by_example(
              outputs[-1], targets[:bucket[1]], weights[:bucket[1]],
              softmax_loss_function=softmax_loss_function))
          else:
            losses.append(tf.nn.seq2seq.sequence_loss(
              outputs[-1], targets[:bucket[1]], weights[:bucket[1]],
              softmax_loss_function=softmax_loss_function))
        else:
          my_inputs = [[],[]]
          my_inputs[0] = encoder_inputs[0][:bucket[0]]
          # NOTE: Sentence length, in words, is ALWAYS 12
          max_nr_of_doc_words = bucket[1] * sentence_length
          my_inputs[1] = encoder_inputs[1][:max_nr_of_doc_words]

          aSoftmaxes, (bucket_outputs, _) = seq2seq(my_inputs,
                                                    decoder_inputs[:bucket[2]])
          
          outputs.append(bucket_outputs)
          if per_example_loss:
            losses.append(tf.nn.seq2seq.sequence_loss_by_example(
              outputs[-1], targets[:bucket[2]], weights[:bucket[2]],
              softmax_loss_function=softmax_loss_function))
          else:
            print("outputs[-1]: ", outputs[-1])
            #losses.append(tf.nn.seq2seq.sequence_loss(
            #losses.append(tf.contrib.seq2seq.sequence_loss(
            losses.append(sequence_loss(
              outputs[-1], targets[:bucket[2]], weights[:bucket[2]],
              softmax_loss_function=softmax_loss_function))

        all_softmaxes.append(aSoftmaxes)

  return outputs, losses, all_softmaxes
