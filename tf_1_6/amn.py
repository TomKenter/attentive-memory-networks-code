# Copyright 2017 Tom Kenter.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""
This code implements the model described in Attentive Memory Networks: Efficient Machine Reading for Conversational Search, Tom Kenter and Maarten de Rijke, Proceedings of 1st International Workshop on Conversational Approaches to Information Retrieval, at SIGIR 2017, Tokyo, Japan, August 11, 2017 (CAIR'17).

If you use this software in your research, please cite the paper.

This is the bibtex:

@inproceedings{amn2017kenter,
  title={Attentive Memory Networks: Efficient Machine Reading for Conversational Search},
  author={Tom Kenter and Maarten de Rijke},
  booktitle={Proceedings of the 1st International Workshop on Conversational Approaches to Information Retrieval (CAIR'17) at the 40th International ACM SIGIR Conference on Research and Development in Information Retrieval (SIGIR 2017)},
  webpdf={https://drive.google.com/file/d/0B2W_GAE99aJlTUU5VTk3YTktTzQ/view},
  year={2017}
}

The code is based on models/tutorials/rnn/translate/translate.py in the
Tensorflow distribution (see https://github.com/tensorflow/models/blob/master/tutorials/rnn/translate/translate.py).

Like the translate mdoel, we use a model with buckets. In the paper we test the model on the 20 bAbi QA datasets (https://research.fb.com/downloads/babi/). For convenience, we predefined the buckets and the vocabulary sizes for the bAbi sets below. If other input is to be used, it should be easy to change or override these defaults..

The structure of the code is kept similar to the original translate code, for easy comparison. This file contains the main training and test loops. In mySeq2seqModel.py the Tensorflow code is set up, and in amnModel.py the actual Attentive Memory Network is implemented.
The file myDataUtils.py contains the code for reading the input files.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import os
import sys
import time
import re

import numpy as np
from six.moves import xrange
import tensorflow as tf

import myDataUtils as data_utils
import mySeq2seqModel as seq2seq_model

tf.app.flags.DEFINE_float("learning_rate", 0.5, "Learning rate.")
tf.app.flags.DEFINE_float("learning_rate_decay_factor", 0.5,
                          "Learning rate decays by this much (default: 0.5).")
tf.app.flags.DEFINE_float("max_gradient_norm", 1.0,
                          "Clip gradients to this norm.")
tf.app.flags.DEFINE_float("output_keep_prob", 1.0,
                          "Dropout. Output keep probability.")
tf.app.flags.DEFINE_integer("batch_size", 50,
                            "Batch size to use during training.")
tf.app.flags.DEFINE_integer("size", 1024, "Size of each model layer.")
tf.app.flags.DEFINE_integer("embedding_size", 256, "Size of embeddings.")
tf.app.flags.DEFINE_integer("num_layers", 3, "Number of layers in the model.")
tf.app.flags.DEFINE_integer("num_memories", -1, "Number of memories.")
tf.app.flags.DEFINE_integer(
  "num_heads", 1,
  "Number of attention heads that read from attention states")
tf.app.flags.DEFINE_string(
  "train_path", "train",
  "Train path (the files <train_path>.source and <train_path>.target should "
  "exist")
tf.app.flags.DEFINE_string(
  "dev_path", "dev",
  "Dev path (the files <dev_path>.source and <dev_path>.target should exist")
tf.app.flags.DEFINE_string(
  "test_path", "test",
  "Test path (the files <test_path>.source and <dev_path>.target should exist")
tf.app.flags.DEFINE_string("checkpoints_save_dir", "/tmp",
                           "Directory to store checkpoints to.")
tf.app.flags.DEFINE_string(
  "job_id", '',
  "Provide this if this script is ran form within another script.")
tf.app.flags.DEFINE_string("checkpoint_file", '',
                           "Prefix of the path of a checkpoint file to load "
                           "(e.g.: myCheckpoints/.../checkpoint-2400).")
tf.app.flags.DEFINE_string(
  "decode_vocab_file_prefix", "vocab_prefix",
  "Prefix of the vocab files (<decode_vocab_file_prefix>.source and "
  "<decode_vocab_file_prefix>.target) to be used by the decoder.")
tf.app.flags.DEFINE_string("qa_set", '',
                           "Name of the qa set to use (so, e.g., qa15")
tf.app.flags.DEFINE_string("optimizer", 'sgd',
                           "Optimization algorithm to use ('sgd' or 'adam')")
tf.app.flags.DEFINE_integer(
  "max_train_data_size", 0,
  "Limit on the size of training data (0: no limit).")
tf.app.flags.DEFINE_integer("sentence_length", 0,
                           "Maximum number of words per sentence")
tf.app.flags.DEFINE_integer("steps_per_checkpoint", 200,
                            "How many training steps to do per checkpoint.")
tf.app.flags.DEFINE_integer("max_steps", 1000,
                            "Maximum number of steps (default 1000.")
tf.app.flags.DEFINE_integer(
  "num_softmax_samples", -1,
  "Number of samples to take for sampled softmax (-1 (default) means: don't "
  "use sampled softmax)")
tf.app.flags.DEFINE_boolean(
  "test", False, "Test a model (presumably pre-trained) on a test set.")
tf.app.flags.DEFINE_boolean(
  "print_attentions", False,
  "Print all sentences and the attentions over them.")
tf.app.flags.DEFINE_boolean("use_fp16", False,
                            "Train using fp16 instead of fp32.")
tf.app.flags.DEFINE_boolean("force", False,
                            "Don't exit on already existing result directoy.")
tf.app.flags.DEFINE_boolean("debug", False, "Debugger mode.")
tf.app.flags.DEFINE_boolean("v", False, "Be verbose.")
tf.app.flags.DEFINE_boolean("vv", False, "Be very verbose.")

FLAGS = tf.app.flags.FLAGS

# We use a number of buckets and pad to the closest one for efficiency.
# See seq2seq_model.Seq2SeqModel for details of how they work.

iSentenceLength = 12

dBuckets = {
  'qa1': [(3+1, 12, 3)],
  'qa2': [(4+1, 20, 3), (4+1, 30, 3), (4+1, 40, 3), (4+1, 50, 3), (4+1, 60, 3),
          (4+1, 70, 3), (4+1, 90, 3)],
  # We read up to 130 sentences of this set
  'qa3': [(7+1, 33, 3), (7+1, 66, 3), (7+1, 99, 3), (7+1, 132, 3)],
  'qa4': [(6+1, 4, 3)],
  'qa5': [(7+1, 20, 3), (7+1, 40, 3), (7+1, 66, 3), (7+1, 80,3), (7+1, 104,3),
          (7+1, 128, 3) ],
  'qa6': [(5+1, 16, 3), (5+1, 28, 3)],
  'qa7': [(6+1, 12, 3), (6+1, 24, 3), (6+1, 36, 3), (6+1, 48, 3), (6+1, 54, 3)],
  'qa8': [(4+1, 16, 7), (4+1, 32, 7), (4+1, 48, 5), (4+1, 64, 7)],
  'qa9': [(5+1, 12, 3)],
  'qa10': [(5+1, 12, 3)],
  'qa11': [(3+1, 12, 3)],
  'qa12': [(3+1, 12, 3)],
  'qa13': [(3+1, 12, 3)],
  'qa14': [(6+1, 16, 3)],
  'qa15': [(5+1, 10, 3)],
  'qa16': [(4+1, 12, 3)],
  'qa17': [(11+1, 4, 3)],
  'qa18': [(9+1, 16, 3), (9+1, 24, 3)],
  'qa19': [(10+1, 8, 5)],
  'qa20': [(7+1, 14, 3)]
}

dVocabSize = {
  "qa1": 20 + 4, 
  "qa2": 34 + 4,
  "qa3": 35 + 4,
  "qa4": 15 + 4,
  "qa5": 40 + 4,
  "qa6": 36 + 4,
  "qa7": 44 + 4,
  "qa8": 37 + 4,
  "qa9": 24 + 4,
  "qa10": 25 + 4,
  "qa11": 27 + 4,
  "qa12": 21 + 4,
  "qa13": 27 + 4,
  "qa14": 26 + 4,
  "qa15": 18 + 4,
  "qa16": 18 + 4,
  "qa17": 19 + 4,
  "qa18": 19 + 4,
  "qa19": 25 + 4,
  "qa20": 36 + 4,
}

# Global variable for the buckets.
_buckets = None


def whitespace_tokenizer(sSentence):
  """Very basic tokenizer: split the sentence into a list of tokens.

  Args:
    sSentence: Sentence as a string"

  Returns:
    aTokens: a list of tokens (strings).
  """
  words = []
  aTokens = [x for x in sSentence.strip().split(' ') if x != '']
  return aTokens


def float2str(f):
  return re.sub("_?0+$", '', ("%f" % f).replace(".", "_"))


def evaluate_predictions(npaPredictions, npaGroundTruths):
  """Compare predicted values to ground truth.
  
  

  Args:
    npaPredictions: Numpy array of predictions.
    npaGroundTruths: Numpy array of ground truth values.

  Returns:
    nr_of_correct_answers: integer.
  """
  nr_of_correct_answers = 0

  # We are doing this example by example :-/.
  for iCol in range(npaPredictions.shape[1]):
    for iRow in range(npaPredictions.shape[0]):
      if npaPredictions[iRow, iCol] != npaGroundTruths[iRow,iCol]:
        break
      # Here, we know that the prediction agrees with the ground truth 
      if npaPredictions[iRow, iCol] == data_utils.EOS_ID:
        nr_of_correct_answers += 1
        break

  return nr_of_correct_answers


def checkTrainDevSize(train_total_size, dev_total_size):
  """Sanity check of training/dev set.

  Args:
    train_total_size: total number of examples in training set.
    dev_total_size: total number of examples in dev set.

  If the numbers are wrong this function exits.
  """

  print( "train: %d, dev: %d" % (train_total_size, dev_total_size))

  check_train_total, check_dev_total = (9000, 1000)

  if train_total_size != check_train_total:
    print("[ERROR] not %d training examples: %d" % (check_train_total,
                                                    train_total_size),
          file=sys.stderr)
    exit(1)

  if dev_total_size != check_dev_total:
    print("[ERROR] not %d training examples: %d" % (check_dev_total,
                                                    dev_total_size),
          file=sys.stderr)
    exit(1)


def read_data(source_questions_path, source_docs_path,
              target_path, max_size=None, mode='training'):
  """Read data from source and target files and put into buckets.

  Args:
    source_questions_path: Path to the files with token-ids for the questions.
    source_docs_path: Path to the files with token-ids for the documents.
    target_path: Path to the file with token-ids for the target language;
      it must be aligned with the source file: n-th line contains the desired
      output for n-th line from the source_path.
    max_size: Maximum number of lines to read, all other will be ignored;
      if 0 or None, data files will be read completely (no limit).
    mode: Either 'training' or 'dev'. 

  Returns:
    data_set: a list of length len(_buckets); data_set[n] contains a list of
      (source, target) pairs read from the provided data files that fit
      into the n-th bucket, i.e., such that len(source) < _buckets[n][0] and
      len(target) < _buckets[n][1]; source and target are lists of token-ids.
  """
  data_set = [[] for _ in _buckets]

  source_questions_file = tf.gfile.GFile(source_questions_path, mode="r")
  source_docs_file = tf.gfile.GFile(source_docs_path, mode="r")  
  target_file = tf.gfile.GFile(target_path, mode="r")

  source_question, source_doc, target = (source_questions_file.readline(),
                                         source_docs_file.readline(),
                                         target_file.readline())
  counter = 0
  while source_question and source_doc and target and \
        (not max_size or counter < max_size):
    counter += 1
    if counter % 100000 == 0:
      print("  reading data line %d" % counter)
      sys.stdout.flush()

    # Question
    source_question_ids = [int(x) for x in source_question.split()]

    # Document
    source_doc_ids = []
    sentence_idx = 0
    aSourceDocIds = source_doc.split()
    source_doc_idxs = [int(x) for x in aSourceDocIds]

    # bAbi files are split by _DOTs
    split_token_idx = source_doc_idxs[-1]

    # We are building a list of lists here (a list of id lists for every
    # sentence)
    for idx in source_doc_idxs:
      if idx == split_token_idx: # The split token itself is ignored

        # If it's too long, only keep the first max_sentence_length words 
        if len(source_doc_ids[sentence_idx]) > FLAGS.sentence_length:
          source_doc_ids[sentence_idx] = \
            source_doc_ids[sentence_idx][:FLAGS.sentence_length]

        # If it's too short, pre-pad
        if len(source_doc_ids[sentence_idx]) < FLAGS.sentence_length:
          padding = [data_utils.PAD_ID] * (FLAGS.sentence_length -
                                           len(source_doc_ids[sentence_idx]))
          source_doc_ids[sentence_idx] = \
            padding + source_doc_ids[sentence_idx]

        sentence_idx += 1
      elif len(source_doc_ids) == sentence_idx:
        source_doc_ids.append([idx])
      else:
        source_doc_ids[sentence_idx].append(idx)

    candidate_ids = None
    target_ids = [int(x) for x in target.split()]
    target_ids.append(data_utils.EOS_ID)

    if mode == 'dev': # Put everything in one (the last) bucket
      enum_buckets = [(len(_buckets) -1, _buckets[-1])]
    else:
      enum_buckets = enumerate(_buckets)

    bSuccess = False
    for bucket_id, (source_question_size, source_doc_size, target_size) in \
        enum_buckets:
      if len(source_question_ids) < source_question_size and \
         len(source_doc_ids) < source_doc_size and \
         len(target_ids) < target_size:
        # Pad the question
        # NOTE: we pre-pad. First padding, then the question itself
        padding = [data_utils.PAD_ID] * (source_question_size - 
                                         len(source_question_ids))
        source_question_ids = padding + source_question_ids

        # Pad the sentences
        # NOTE: we pre-pad here. First the padded/empty sentences, than the
        # story itself
        flat_source_doc_ids = sum(source_doc_ids, []) # Flatten list of lists
        padding = [data_utils.PAD_ID] * (
          (FLAGS.sentence_length * source_doc_size) - \
          len(flat_source_doc_ids))
        flat_source_doc_ids = padding + flat_source_doc_ids

        data_set[bucket_id].append(
          [source_question_ids, flat_source_doc_ids, target_ids,
           candidate_ids])
        bSuccess = True
        break

    if not bSuccess:
      print("[ERROR] didn't find a fitting bucket for line %d in %s or %s" % (
        counter, source_path, additional_source_path), file=sys.stderr)
      print("[ERROR] source question size: %d, source document size: %d, "
            "target size: %d, buckets: %s" % (
              len(source_question_ids), len(source_doc_ids), len(target_ids),
              _buckets), file=sys.stderr)
      exit(1)

    source_question, source_doc, target = (source_questions_file.readline(),
                                           source_docs_file.readline(),
                                           target_file.readline())
  source_questions_file.close()
  source_docs_file.close()
  target_file.close()

  return data_set


def create_model(session, forward_only):
  """Create an Attentive Memory Network model.

  Args:
    session: Tensorflow session.
    forward_only: If True, no learning step is carried out.

  Returns:
    model: Attentive Memory Network model.
  """

  """Create translation model and initialize or load parameters in session."""
  dtype = tf.float16 if FLAGS.use_fp16 else tf.float32
  model = seq2seq_model.Seq2SeqModel(
    dVocabSize[FLAGS.qa_set], # source_vocab_size
    dVocabSize[FLAGS.qa_set], # target_vocab_size
    FLAGS.sentence_length,
    _buckets,
    FLAGS.size,
    FLAGS.embedding_size,
    FLAGS.num_layers,
    FLAGS.num_memories,
    FLAGS.num_heads,
    FLAGS.max_gradient_norm,
    FLAGS.output_keep_prob,
    FLAGS.batch_size,
    FLAGS.learning_rate,
    FLAGS.learning_rate_decay_factor,
    FLAGS.optimizer,
    num_samples=FLAGS.num_softmax_samples,
    forward_only=forward_only,
    dtype=dtype,
    debug=FLAGS.debug)

  print("Created model:")
  model.write()

  if len(FLAGS.checkpoint_file):
    print("Reading model parameters from %s" % FLAGS.checkpoint_file)
    model.saver.restore(session, FLAGS.checkpoint_file)
    # Assign fresh learning rate
    model.learning_rate.assign(FLAGS.learning_rate)
  else:
    print("Created model with fresh parameters.")
    tf.global_variables_initializer().run()

  return model

def get_checkpoints_save_dir():
  """Get a directory name to store checkpoints to, based on hyperparameters.

  Returns:
    Full path to a directory, based on the hyperparameters.
  """


  # Print the parameters if running in a batch on the server.
  if len(FLAGS.job_id):
    print("Job id: %s" % FLAGS.job_id)
    print("Learning rate: %f\nLearning rate decay factor: %f\n"
          "Max gradient norm: %f\nOutput keep prob: %f\nBatch size: %d\n"
          "Hidden layer size: %d\nEmbedding size: %d\nNum layers: %d\n"
          "Num memories: %d\nNum heads: %d\nSource vocab size: %d\n"
          "Target vocab size: %d\nOptimizer: %s\nMax sentence length: %d\n"
          "Sampled softmax: %s" % (
            FLAGS.learning_rate, FLAGS.learning_rate_decay_factor,
            FLAGS.max_gradient_norm, FLAGS.output_keep_prob, FLAGS.batch_size,
            FLAGS.size, FLAGS.embedding_size, FLAGS.num_layers,
            FLAGS.num_memories, FLAGS.num_heads,
            dVocabSize[FLAGS.qa_set], # source_vocab_size
            dVocabSize[FLAGS.qa_set], # target_vocab_size
            FLAGS.optimizer, FLAGS.sentence_length,
            "%d" % FLAGS.num_softmax_samples if FLAGS.num_softmax_samples > 0 \
            else "no" 
          ))

  checkpoints_save_dir = FLAGS.checkpoints_save_dir

  # We create a sub directory 
  custom_save_dir = "jobid_%s_" % FLAGS.job_id if len(FLAGS.job_id) else ''
  custom_save_dir += "lr_%s_lrd_%s_opt_%s_okp_%s_mgn_%s_bs_%d_sz_%d_esz_%d" % (
    float2str(FLAGS.learning_rate),
    float2str(FLAGS.learning_rate_decay_factor), FLAGS.optimizer,
    float2str(FLAGS.output_keep_prob), float2str(FLAGS.max_gradient_norm),
    FLAGS.batch_size, FLAGS.size, FLAGS.embedding_size)
  custom_save_dir += "_nl_%d_nm_%d_nmh_%d_svs_%d_tvs_%d_msl_%d_nms_%s" % (
    FLAGS.num_layers,
    FLAGS.num_memories, FLAGS.num_heads,
    dVocabSize[FLAGS.qa_set], # source_vocab_size
    dVocabSize[FLAGS.qa_set], # target_vocab_size
    FLAGS.sentence_length,
    "%d" % FLAGS.num_softmax_samples if FLAGS.num_softmax_samples > 0 \
    else "no"
  )

  return os.path.join(checkpoints_save_dir, custom_save_dir)


def initialize_paths():
  """Initialize all necessary path variables from the command line flags.

  Returns:
    source_train_ids_path_questions: Path of a training file with token ids of
      questions.
    source_train_ids_path_docs: Path of a training file with token ids of
      documents.
    source_train_ids_path_qdocs: Path of a training file with token ids of
      questions and documents (only used in standard seq2seq scenario).
    target_train_ids_path: Path of a training file with token ids of the
      targets (i.e. the answers). 
    source_dev_ids_path_questions: Path of a dev file with token ids of
      questions.
    source_dev_ids_path_docs: Path of a dev file with token ids of documents.
    source_dev_ids_path_qdocs: Path of a dev file with token ids of questions
      and documents (only used in standard seq2seq scenario).
    target_dev_ids_path: Path of a dev file with token ids of the targets (i.e.
      the answers).
    source_vocab_path: Path of a source vocabulary file. 
    target_vocab_path: Path of a target vocabulary file. 
    checkpoints_save_dir: Directory to store checkpoints in.
  """
  print("Working directory: %s" % os.getcwd())
  print("Preparing train/dev data in %s, %s" % (
    FLAGS.train_path, FLAGS.dev_path))

  source_train_ids_path_questions, source_train_ids_path_docs, \
    source_train_ids_path_qdocs, target_train_ids_path, \
    source_dev_ids_path_questions, source_dev_ids_path_docs, \
    source_dev_ids_path_qdocs, target_dev_ids_path, \
    source_vocab_path, target_vocab_path = data_utils.prepare_data(
      FLAGS.train_path, FLAGS.dev_path,
      dVocabSize[FLAGS.qa_set], # source_vocab_size
      dVocabSize[FLAGS.qa_set], # target_vocab_size
      tokenizer=whitespace_tokenizer)

  checkpoints_save_dir = get_checkpoints_save_dir()

  if os.path.exists(checkpoints_save_dir):
    if FLAGS.force:
      print("[WARNING] path %s already exists" % checkpoints_save_dir,
            file=sys.stderr)
    else:
      print("[ERROR] path %s already exists" % checkpoints_save_dir,
            file=sys.stderr)
      exit(1)
  else:
    print("Creating path checkpoints_save_dir '%s'" % checkpoints_save_dir) 
    os.mkdir(checkpoints_save_dir)

  return source_train_ids_path_questions, source_train_ids_path_docs, \
    source_train_ids_path_qdocs, target_train_ids_path, \
    source_dev_ids_path_questions, source_dev_ids_path_docs, \
    source_dev_ids_path_qdocs, target_dev_ids_path, \
    source_vocab_path, target_vocab_path, checkpoints_save_dir


def print_progress(sess, model, loss, current_step, step_time, dev_set,
                   rev_source_vocab, rev_target_vocab, checkpoints_save_dir,
                   dProgressStats):
  """Print statistics regarding the progress of training process.

  This function runs the current version of the model on the eval set and
  prints the accuracy.

  Args:
    sess: Tensorflow session.
    model: Attentive Memory Network model.
    loss: Current loss.
    current_step: Number of the current step.
    step_time: Time the last step took.
    dev_set: Development set.
    rev_source_vocab: Source vocabulary.
    rev_target_vocab: Target vocabulary.
    checkpoints_save_dir: Directory to load the last checkpoint from.
    dProgressStats: Dictionary holding some running stats.
  """
  # Print statistics for the previous epoch.
  perplexity = math.exp(float(loss)) if loss < 300 else float("inf")
  print ("global step %d learning rate %.4f step-time %.2f perplexity "
         "%.2f loss: %.12f" % (
           model.global_step.eval(), model.learning_rate.eval(),
           step_time, perplexity, loss))

  # Decrease learning rate if no improvement was seen over last 3 times.
  if len(dProgressStats["previous_losses"]) > 2 and \
     loss > max(dProgressStats["previous_losses"][-3:]):
    print("[ANNEALING] because loss is not improving")
    sess.run(model.learning_rate_decay_op)
    dProgressStats["previous_losses"] = [] # Reset
  dProgressStats["previous_losses"].append(loss)

  fPerplexity, fAccuracy = evaluate_model(
    sess, model, dev_set, rev_source_vocab, rev_target_vocab, label="dev")

  # Early stopping
  if (len(dProgressStats["previous_identical_ppxs"]) > 5) and (loss < 0.00001):
    print("[QUITING] Perplexity stays the same. "
          "Nothing is being learnt anymore")
    print("Best accuracy achieved on dev set: %f" % \
          dProgressStats["best_accuracy"])
    exit(0)

  # Only save checkpoints if they are worth it, in terms of accuracy.
  if fAccuracy > dProgressStats["best_accuracy"]:
    removeCheckpoints(checkpoints_save_dir)
    saveCheckpoint(checkpoints_save_dir, sess, model, current_step)
    dProgressStats["best_accuracy"] = fAccuracy
  elif fAccuracy > 0 and (fAccuracy == dProgressStats["best_accuracy"]):
    saveCheckpoint(checkpoints_save_dir, sess, model, current_step)

  # If the perplexity on the development set is increasing three times in
  # row
  if (dProgressStats["previous_ppx"] is None) or \
     (fPerplexity < dProgressStats["previous_ppx"]):
    dProgressStats["ppx_development"] = 0
    dProgressStats["previous_identical_ppxs"] = []
  elif fPerplexity == dProgressStats["previous_ppx"]:
    # NOTE: If the ppx stays the same, we keep the
    # dProgressStats["ppx_development"] as is.
    dProgressStats["previous_identical_ppxs"].append(fPerplexity)
  else: # fPerplexity > dProgressStats["previous_ppx"]
    dProgressStats["ppx_development"] += 1
    dProgressStats["previous_identical_ppxs"] = []

  print("Perplexity development: %d" % dProgressStats["ppx_development"])

  if dProgressStats["ppx_development"] == 3:
    print("[ANNEALING] because of increasing dev perplexity")
    sess.run(model.learning_rate_decay_op)
    dProgressStats["ppx_development"] = 0 # Reset


def init_data():
  """Load training set, dev set, and vocabularies.

  Returns:
    rev_source_vocab: Source vocabulary.
    rev_target_vocab: Target vocabulary.
    train_set: Training set.
    dev_set: Development set.
    train_buckets_scale: Partitioning of the 0 to 1 scale, proportional to the
      bucjket sizes.
    checkpoints_save_dir: Directory to save checkpoints to.
  """
  source_train_ids_path_questions, source_train_ids_path_docs, \
    source_train_ids_path_qdocs, target_train_ids_path, \
    source_dev_ids_path_questions, source_dev_ids_path_docs, \
    source_dev_ids_path_qdocs, target_dev_ids_path, \
    source_vocab_path, target_vocab_path, \
    checkpoints_save_dir = initialize_paths()

  # We want the vocabs for printing the dev examples
  _, rev_source_vocab = data_utils.initialize_vocabulary(source_vocab_path)
  _, rev_target_vocab = data_utils.initialize_vocabulary(target_vocab_path)

  # Read data into buckets and compute their sizes.
  print("Reading development and train data (limit: %d)." % \
        FLAGS.max_train_data_size)

  train_set = read_data(source_train_ids_path_questions,
                        source_train_ids_path_docs,
                        target_train_ids_path,
                        max_size=FLAGS.max_train_data_size)
  dev_set = read_data(source_dev_ids_path_questions,
                      source_dev_ids_path_docs,
                      target_dev_ids_path,
                      mode="dev")

  train_bucket_sizes = [len(train_set[b]) for b in xrange(len(_buckets))]
  train_total_size = float(sum(train_bucket_sizes))
  dev_bucket_sizes = [len(dev_set[b]) for b in xrange(len(_buckets))]
  dev_total_size = float(sum(dev_bucket_sizes))

  print("Buckets: %s" % _buckets)
  print("Train total size: %d" % train_total_size)
  print("Train bucket sizes: %s" % train_bucket_sizes)
  print("Dev bucket sizes: %s" % dev_bucket_sizes)
  print("Dev total size: %d" % dev_total_size)

  checkTrainDevSize(train_total_size, dev_total_size)

  # A bucket scale is a list of increasing numbers from 0 to 1 that we'll use
  # to select a bucket. Length of [scale[i], scale[i+1]] is proportional to
  # the size if i-th training bucket, as used later.
  train_buckets_scale = [sum(train_bucket_sizes[:i + 1]) / train_total_size
                         for i in xrange(len(train_bucket_sizes))]

  print ("Train bucket sizes: %s. Sum = %d" % (train_bucket_sizes,
                                           sum(train_bucket_sizes)))
  print ("Train total size: %s" % train_total_size)
  print ("Train buckets scale: %s" % train_buckets_scale)

  return (rev_source_vocab, rev_target_vocab, train_set, dev_set,
          train_buckets_scale, checkpoints_save_dir)

def train():
  """Training loop.

  An Attentibe Memory Network  model is built, run on the training data and
  evaluated on the development data.
  """
  
  step_time, loss = 0.0, 0.0
  current_step = 0
  dProgressStats = {
    "previous_losses": [],
    "previous_identical_ppxs": [],
    "previous_ppx": None,
    "best_accuracy": 0,
    "ppx_development": 0
  }

  (rev_source_vocab, rev_target_vocab, train_set, dev_set,
   train_buckets_scale, checkpoints_save_dir) = init_data()

  with tf.Session() as sess:
    # Create model.
    print("Creating %d layers of %d units." % (FLAGS.num_layers, FLAGS.size))
    model = create_model(sess, False)

    print("Start training")
    while current_step < FLAGS.max_steps:
      current_step += 1

      # Choose a bucket according to data distribution. We pick a random number
      # in [0, 1] and use the corresponding interval in train_buckets_scale.
      random_number_01 = np.random.random_sample()
      bucket_id = min([i for i in xrange(len(train_buckets_scale))
                       if train_buckets_scale[i] > random_number_01])

      # Get a batch and make a step.
      start_time = time.time()
      encoder_inputs, decoder_inputs, target_weights, candidate_ids = \
        model.get_batch(train_set, bucket_id)

      _, step_loss, _, _ = model.step(sess, encoder_inputs, decoder_inputs,
                                      target_weights, bucket_id, False)
      step_time += (time.time() - start_time) / FLAGS.steps_per_checkpoint
      loss += step_loss / FLAGS.steps_per_checkpoint

      # Once in a while, we save checkpoint, print statistics, and run evals.
      if current_step % FLAGS.steps_per_checkpoint == 0:
        print_progress(sess, model, loss, current_step, step_time, dev_set,
                       rev_source_vocab, rev_target_vocab,
                       checkpoints_save_dir, dProgressStats)

        # Reset.
        step_time, loss = 0.0, 0.0

    print("Done training. Best accuracy achieved on dev set: %f" % \
          dProgressStats["best_accuracy"])


def removeCheckpoints(checkpoints_save_dir):
  """Delete all checkpoints in the checkpoints saving directory.

  This is called when a the new version of the model is better than the
  previously stored version(s).
  """

  for checkpoint_file in [f for f in os.listdir(checkpoints_save_dir) if os.path.isfile(os.path.join(checkpoints_save_dir, f))]:
    del_file = os.path.join(checkpoints_save_dir, checkpoint_file)
    if FLAGS.vv:
      print("Deleting %s" % del_file)
    os.remove(del_file)


def saveCheckpoint(checkpoints_save_dir, sess, model, current_step):
  """Save a checkpoint to disk.

  checkpoints_save_dir: Directory to save the checkpoint to.
  sess: Tensorflow session.
  model: Attentive Memory Networks model.
  current_step: Step number.
  """


  # Save checkpoint and zero timer and loss.
  checkpoint_path = os.path.join(checkpoints_save_dir, "checkpoint")
  if FLAGS.vv:
    print("Saving checkpoint at step %d to %s" % (current_step,
                                                  checkpoints_save_dir))
    
  model.saver.save(sess, checkpoint_path, global_step=model.global_step)


def test():
  """Loads a checkpoint and runs it on a test set.

  The performance is printed to stdout.
  """

  with tf.Session() as sess:
    if FLAGS.vv:
      print("Preparing data")

    # As the data is tokenized already, we only need to split on whitespaces
    source_test_ids_path_questions, source_test_ids_path_docs, \
      source_test_ids_path_qdocs, target_test_ids_path, source_vocab_path, \
      target_vocab_path = data_utils.prepare_data_tied(
        FLAGS.test_path,
        dVocabSize[FLAGS.qa_set], # source_vocab_size
        dVocabSize[FLAGS.qa_set], # target_vocab_size
        tokenizer=whitespace_tokenizer)

    # Load vocabularies.
    if FLAGS.vv:
      print("Loading vocabularies '%s', '%s'" % (source_vocab_path,
                                                 target_vocab_path))
    _, rev_source_vocab = data_utils.initialize_vocabulary(source_vocab_path)
    _, rev_target_vocab = _, rev_source_vocab

    if FLAGS.vv:
      print("Reading test data")

    test_set = read_data(source_test_ids_path_questions,
                         source_test_ids_path_docs,
                         target_test_ids_path,
                         mode='dev')

    # Create model and load parameters.
    if FLAGS.vv:
      print("Creating model")    
    model = create_model(sess, True)

    _, _ = evaluate_model(
      sess, model, test_set, rev_source_vocab, rev_target_vocab, label="test")


def get_bucket_stats(sess, model, data_set, bucket_id, rev_source_vocab,
                     rev_target_vocab):
  """Get performance statistics for a bucket.

  Args:
    sess: Tensorflow session.
    model: Attentive Memory Networks model.
    data_set: A data set to evaluate the model on.
    bucket_id: Identifier of the bucket.
    rev_source_vocab: Source vocabulary. 
    rev_target_vocab: Target vocabulary.

  Returns:
    A tuple contain: Loss in this bucket, number of examples checked in this
    bucket, number of correct predictions in this bucket, number of correct
    predictions in this bucket from pre-selected set of candidates.
  """
  fBucketLoss = 0
  iNrCheckedInBucket = 0
  iNrCorrectBucket = 0

  iLenDataSet = len(data_set[bucket_id])
  if (iLenDataSet % model.batch_size) != 0:
    print(
      "[WARNING] batch_size %d " % model.batch_size,
      "doesn't perfectly fit the number of dev examples, %d" % iLenDataSet,
      file=sys.stderr)

  iMaxRange = iLenDataSet - (iLenDataSet % FLAGS.batch_size)
  for iStartAt in xrange(0, iMaxRange, FLAGS.batch_size):
    encoder_inputs, decoder_inputs, target_weights, candidate_ids = \
      model.get_batch(data_set, bucket_id, mode='deterministic',
                      iStartAt=iStartAt)

    # softmaxes: list of lists:
    #   [nr_of_memories * nr_of_heads * tensor(batch x nr_of_sentences)]
    _, eval_loss, output_logits, softmaxes = model.step(
      sess, encoder_inputs, decoder_inputs, target_weights, bucket_id, True)
    fBucketLoss += eval_loss

    iNrCheckedInBatch = len(output_logits[0])
    iNrCheckedInBucket += iNrCheckedInBatch

    iNrOfCorrectAnswersBatch, iNrOfCorrectFromSelection = \
      getNrOfCorrectAnswers(sess, model, output_logits, encoder_inputs,
                            decoder_inputs, rev_source_vocab,
                            rev_target_vocab, candidate_ids,
                            softmaxes)
    iNrCorrectBucket += iNrOfCorrectAnswersBatch

  return (fBucketLoss, iNrCheckedInBucket, iNrCorrectBucket,
          iNrOfCorrectFromSelection)


def evaluate_model(sess, model, data_set, rev_source_vocab, rev_target_vocab,
                   label="eval"):
  """Evaluate a model on dev/test set.

  The perplexity and accuracy are printed.

  Args:
    sess: Tensorflow session.
    model: Attentive Memory Networks model.
    data_set: Data set to evaluate.
    rev_source_vocab: Source vocabulary.
    rev_target_vocab: Target vocabulary.
    label: For printing. Either 'dev' or 'test'

  Returns:
    Tuple of two floats average perplexity and accuracy.
  """
  iModelStep = model.global_step.eval()
  iTotalCorrectAnswers, iTotalCorrectFromSelection = 0, None
  iTotalChecked, iTotalBucketsChecked, fTotalPerplexity = 0, 0, 0
  fTotalLoss = 0.0

  for bucket_id in xrange(len(_buckets)):
    if len(data_set[bucket_id]) == 0:
      print("%s: empty bucket %d" % (label, bucket_id))
      continue

    (fBucketLoss, iNrCheckedInBucket, iNrCorrectBucket,
     iNrOfCorrectFromSelection) = get_bucket_stats(
       sess, model, data_set, bucket_id, rev_source_vocab, rev_target_vocab)

    iTotalChecked += iNrCheckedInBucket
    iTotalCorrectAnswers += iNrCorrectBucket
    fTotalLoss += fBucketLoss
    iTotalBucketsChecked += 1
    eval_ppx = math.exp(float(fBucketLoss / iNrCheckedInBucket)) \
               if fBucketLoss < 300 else float("inf")
    fTotalPerplexity += eval_ppx
    print("  %s: bucket %d perplexity %.2f" % (label, bucket_id, eval_ppx))
    print("                 accuracy   %.2f (%d/%d)" % (
      float(iNrCorrectBucket) / iNrCheckedInBucket,
      iNrCorrectBucket, iNrCheckedInBucket) ) 

    if iNrOfCorrectFromSelection is not None:
      if iTotalCorrectFromSelection is None:
        iTotalCorrectFromSelection = iNrOfCorrectFromSelection
      else:
        iTotalCorrectFromSelection += iNrOfCorrectFromSelection

  fLossPerExample = fTotalLoss / iTotalChecked
  fAveragePerplexity = math.exp(float(fLossPerExample)) \
                       if fLossPerExample < 300 else float("inf")
  print( "average perplexity: %f" % fAveragePerplexity)
  fOverallAccuracy = None
  fOverallAccuracy = float(iTotalCorrectAnswers) / iTotalChecked
  print( "[step %d] overall accuracy:\t%f (%d/%d)" % (
    iModelStep, fOverallAccuracy, iTotalCorrectAnswers, iTotalChecked))

  fOverallAccuracyFromSelection = None
  if iTotalCorrectFromSelection is not None:
    fOverallAccuracyFromSelection = float(iTotalCorrectFromSelection) / \
                                    iTotalChecked
    print( "[step %d] accuracy on selection:\t%f (%d/%d)" % (
      iModelStep, fOverallAccuracyFromSelection, iTotalCorrectFromSelection,
      iTotalChecked))
  sys.stdout.flush()

  # NOTE: if there is an accuracy from selection, we go by that, If not take
  #       the overall accuracy
  fAccuracyToReturn = fOverallAccuracy \
                      if fOverallAccuracyFromSelection is None \
                         else fOverallAccuracyFromSelection
  return fAveragePerplexity, fAccuracyToReturn


# npaSoftmaxes is [num_memories * 1 * num_sentences]
# TODO: the 1 should actually be num_heads, but we leave that for now 
def printAttentions(npaSoftmaxes, npaSentences, aRevVocab):
  """Print the attention vectors.

  The attention vectors can be used for visualization purposes.

  Args:
    npaSoftmaxes: Numpy aray of attention vectors to print.
    npaSentences: Sentences the attention vectors correspond to.
    aRevVocab: Vocabulary corresponding the identifiers in npaSentences.
  """
  assert (npaSoftmaxes.shape[2] == \
          npaSentences.shape[0] / FLAGS.sentence_length), \
    "[ERROR] softmax is %d, but we have %d sentences" % (
      npaSoftmaxes.shape[2], npaSentences.shape[0] / FLAGS.sentence_length)

  for i in range(npaSoftmaxes.shape[2]):
    sProbabilities = ""
    sSep = ""
    for m in range(FLAGS.num_memories):
      sProbabilities += "%s%f" % (sSep, npaSoftmaxes[m][0][i])
      sSep = " "
    aSentenceIds = \
      npaSentences[i*FLAGS.sentence_length:(i+1)*FLAGS.sentence_length]
    print("%s || %s" % (
      sProbabilities, prePaddedIds2txt(aSentenceIds, aRevVocab)))

def prePaddedIds2txt(aIds, aRevVocab):
  """Convert a list of ids to a string.

  Args:
    aIds: List of identifiers.
    aRevVocab: Vocabulary corresponding to the identifiers in aIds.

  Returns:
    String representation of the identifiers in the input list.
  """

  iLastPadIndex = 0
  for idx in range(len(aIds)):
    if aIds[idx] == data_utils.PAD_ID:
      iLastPadIndex += 1
    else:
      break

  return " ".join([tf.compat.as_str(aRevVocab[idx]) \
                   for idx in aIds[iLastPadIndex:]])


def getNrOfCorrectAnswers(sess, model, output_logits, encoder_inputs,
                          decoder_inputs, rev_source_vocab, rev_target_vocab,
                          candidate_ids, softmaxes):
  """Get the number of correct predictions.

  Args:
    sess: Tensorflow session.
    model: Attentive Memory Networks model.
    output_logits: Predictions.
    encoder_inputs: Identifiers of input strings.
    decoder_inputs: Identifiers of output strings.
    rev_source_vocab: Source vocabulary.
    rev_target_vocab: Target vocabulary.
    candidate_ids: Pre-selected set of candidate identifiers to evalaute.
    softmaxes: Softmax vectors.

  Returns:
    Tuple of number of correct predictions, number of correct predictions from
    from a pre-selected set of candidate identifiers.
  """

  if model.bSampledSoftmax and not model.forward_only:
    output_logits = [sess.run(tf.matmul(x, model.w) + model.b) \
                     for x in output_logits]

  npaSoftmaxes = np.array(softmaxes) if FLAGS.print_attentions else None
  npaSentences = np.array(encoder_inputs[1]) \
                 if FLAGS.print_attentions else None

  iNrOfCorrectFromSelection = None
  if candidate_ids is not None:
    # aSelectedLogits is [batch_size * nr_of_selected_logits]
    # NOTE that this assumes there is only one answer
    # Can't we do this in one go..?!?
    aSelectedLogits = [output_logits[0][x,candidate_ids[x]] \
                       for x in range(len(output_logits[0]))]

    npaCorrectIndicesInSelection = np.where(
      np.array(candidate_ids) == [[x] for x in decoder_inputs[1]])[1]

    aSelectedOutputIds = [np.argmax(logit,axis=0) for logit in aSelectedLogits]

    iNrOfCorrectFromSelection = np.sum(
      npaCorrectIndicesInSelection == aSelectedOutputIds)

  # Greedy decoder: outputs are just argmaxes of output_logits.  
  # output_logits: list with members of [batch_size, vocab_size] 
  # output_ids:  list with members [batch_size]
  output_ids = [np.argmax(logit, axis=1) for logit in output_logits]
  # Now output_ids is an numpy array: [len(output_logits), batch_size]
  output_ids = np.array(output_ids)
  
  # Same for encoder_inputs
  question_encoder_inputs = np.array(encoder_inputs[0])
  
  # Same for decoder_inputs (which first symbol is always _GO)
  decoder_inputs = np.array(decoder_inputs)
  
  if (FLAGS.qa_set == 'qa8') or (FLAGS.qa_set == 'qa19'):
    # Multiple word answers
    iNrOfCorrectAnswers = evaluate_predictions(output_ids[:-1],
                                               decoder_inputs[1:])
  else:
    # As we >>know<< that the answer should be one word in all cases
    # we can simply do the following
    iNrOfCorrectAnswers = np.sum(
      np.sum(decoder_inputs[[1,2],:] == output_ids[[0,1],:],axis=0) == 2)
  
  if FLAGS.vv:
    # Print out target sentence corresponding to outputs.
    #
    # Transform output_ids to [batch_size, len(output_logits)]  
    output_ids = output_ids.T

    for i in range(output_ids.shape[0]):
      # question_encoder_inputs: [input_steps, batch_size]
      question = prePaddedIds2txt(question_encoder_inputs[:,i],
                                  rev_source_vocab)
      iLastPadIndex = 0
      for idx in range(len(question_encoder_inputs[:,i])):
        if question_encoder_inputs[:,i][idx] == data_utils.PAD_ID:
          iLastPadIndex += 1
        else:
          break

      question = " ".join(
        [tf.compat.as_str(rev_source_vocab[idx]) \
         for idx in question_encoder_inputs[:,i][iLastPadIndex:]])
  
      # Quit after a _EOS
      separator = ''
      predicted_answer = ''
      for idx in output_ids[i]:
        if idx == data_utils.EOS_ID:
          break
        predicted_answer += "%s%s" % (
          separator, tf.compat.as_str(rev_target_vocab[idx]))
        separator = ' '

      separator = ''
      ground_truth_answer = ''
      for idx in decoder_inputs[1:,i]:
        if idx == data_utils.EOS_ID:
          break
        ground_truth_answer += "%s%s" % (
          separator, tf.compat.as_str(rev_target_vocab[idx]))
        separator = ' '

      print ("%d: %s -- %s (gt: %s)" % (
        i, question, predicted_answer, ground_truth_answer))

      if FLAGS.print_attentions:
        if FLAGS.debug:
          import pdb
          pdb.set_trace()
        printAttentions(npaSoftmaxes[:,[0],[i]], npaSentences[:,i],
                        rev_source_vocab)

  return iNrOfCorrectAnswers, iNrOfCorrectFromSelection


def handle_flags():
  """Check if theflags make sense.

  The program exits if the arguments are invalid.  
  """
  if len(FLAGS.qa_set) == 0: 
    # Try to deduct it...
    FLAGS.qa_set = os.path.basename(FLAGS.train_path).split('_')[0]

  if not FLAGS.qa_set.startswith('qa'):
    print("Path at terminal when executing this file")
    print(os.getcwd() + "\n")

    print("This file path, relative to os.getcwd()")
    print(__file__ + "\n")

    print("This file full path (following symlinks)")
    full_path = os.path.realpath(__file__)
    print(full_path + "\n")

    print("This file directory and name")
    path, filename = os.path.split(full_path)
    print(path + ' --> ' + filename + "\n")

    print("This file directory only")
    print(os.path.dirname(full_path))
    print("[ERROR] Can't determine QA set", file=sys.stderr)
    exit(1)

  if not FLAGS.optimizer in ['adam', 'sgd']:
    print("[ERROR] Don't know optimizer '%s'" % FLAGS.optimizer,
          file=sys.stderr)
    exit(1)

  if FLAGS.print_attentions:
    FLAGS.vv = True

  if FLAGS.num_memories <= 0:
    print("[ERROR] The number of memories should be higher than 1.",
          file=sys.stderr)
    exit(1)

  #_buckets = [(18, 3), (30, 3), (44, 3), (57, 3), (70, 3)]
  global _buckets
  if FLAGS.num_memories:
    _buckets = dBuckets[FLAGS.qa_set]
  else:
    _buckets = [(x[1], x[2]) for x in dBuckets[FLAGS.qa_set]]

  if FLAGS.sentence_length == 0:
    # Take the global predefined value if user didn't specify anything
    FLAGS.sentence_length = iSentenceLength


def main(_):
  handle_flags()

  if FLAGS.test:
    test()
  else:
    train()

if __name__ == "__main__":
  tf.app.run()
