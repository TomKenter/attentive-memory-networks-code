# Copyright 2015 Tom Kenter. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from tensorflow.python.ops import control_flow_ops
from tensorflow.python.ops import rnn_cell
from tensorflow.python.ops import init_ops
from tensorflow.python.ops import variable_scope
import tensorflow as tf
import math

import mySeq2seq as seq2seq

def create_embeddings(num_symbols, embedding_size, name="embeddings"):
  # Default initializer for embeddings should have variance=1.
  sqrt = math.sqrt(6)  # Uniform(-sqrt(3), sqrt(3)) has variance=1.
  initializer = init_ops.random_uniform_initializer(-sqrt, sqrt)

  return tf.get_variable(
    name,
    shape=[num_symbols, embedding_size],
    initializer=initializer
  )


def amn(encoder_inputs,
        decoder_inputs,
        cell,
        num_encoder_symbols,
        num_decoder_symbols,
        sentence_length,
        num_memories,
        embedding_size,
        num_heads=1,
        output_projection=None,
        feed_previous=False,
        dtype=None,
        initial_state_attention=False):
  """Attentive Memory Network.

  The Attentive Memory Netwrok implemented here has two encoders, one for the
  question and one for the document. The latter encoder is hierarchical, i.e.,
  the words in the input are encoded into sentence embeddings, which are
  encoded by a (bi-directional) sentence-level RNN.
  Between encodin and decoding, there is a memory module, which is a separate
  RNN, that gets the last state of the question encoder as input at every step,
  and attends over over the document encoder hidden states.
  The output decoder attends over the memory states (i.e., it does NOT attend
  over the document hidden states).

  Args:
    encoder_inputs: Two lists of 1D int32 Tensors of shape [batch_size], one
      for the questions and one for the documents.
    decoder_inputs: A list of 1D int32 Tensors of shape [batch_size].
    cell: rnn_cell.RNNCell defining the cell function and size.
    num_encoder_symbols: Integer; number of symbols on the encoder side.
    num_decoder_symbols: Integer; number of symbols on the decoder side.
    sentence_length: Integer; maximum number of words in a sentence.
    num_memories: Integer; number of memories to use.
    embedding_size: Integer, the length of the embedding vector for each symbol.
    num_heads: Number of attention heads that read from attention_states.
    output_projection: None or a pair (W, B) of output projection weights and
      biases; W has shape [output_size x num_decoder_symbols] and B has
      shape [num_decoder_symbols]; if provided and feed_previous=True, each
      fed previous output will first be multiplied by W and added B.
    feed_previous: Boolean or scalar Boolean Tensor; if True, only the first
      of decoder_inputs will be used (the "GO" symbol), and all other decoder
      inputs will be taken from previous outputs (as in embedding_rnn_decoder).
      If False, decoder_inputs are used as given (the standard decoder case).
    dtype: The dtype of the initial RNN state (default: tf.float32).
    initial_state_attention: If False (default), initial attentions are zero.
      If True, initialize the attentions from the initial state and attention
      states.

  Returns:
    A tuple of the form (outputs, state), where:
      outputs: A list of the same length as decoder_inputs of 2D Tensors with
        shape [batch_size x num_decoder_symbols] containing the generated
        outputs.
      state: The state of each decoder cell at the final time-step.
        It is a 2D Tensor of shape [batch_size x cell.state_size].
  """
  assert (num_memories > 0), "[ERROR] no use to build an Attentive Memory Network if the number of memory is %d" % num_memories

  print("[amnModel.amn] make word encoder.")
  with variable_scope.variable_scope("word_encoder", dtype=dtype) as scope:
    dtype = scope.dtype

    embeddings = create_embeddings(num_encoder_symbols, embedding_size)

    # encoder_inputs is is a list of two lists
    encoder_inputs_question = tf.nn.embedding_lookup(
      embeddings, tf.pack(encoder_inputs[0], axis=1))
    encoder_inputs_doc = tf.nn.embedding_lookup(
      embeddings, tf.pack(encoder_inputs[1], axis=1))

    # Question encoder.
    encoder_outputs_question, encoder_state_question = tf.nn.dynamic_rnn(
      cell, encoder_inputs_question, dtype=dtype, swap_memory=True)

    encoder_inputs_doc = tf.unstack(encoder_inputs_doc, axis=1)

  sentence_states = []
  with variable_scope.variable_scope("word_encoder", dtype=dtype, reuse=True): 
    sentence_states = []
    # Document word encoder.
    # NOTE: sentence length, in words, is fixed.
    for start_idx in range(0, len(encoder_inputs_doc), sentence_length):
      word_encoder_outputs_doc, word_encoder_state_doc = tf.nn.dynamic_rnn(
        cell,
        tf.pack(encoder_inputs_doc[start_idx:start_idx+sentence_length],
                axis=1),
        dtype=dtype, swap_memory=True)

      if type(word_encoder_state_doc) == type(tuple()):
        # If it's a multi-layered cell, encoder_state_question is a tuple of
        # states. We pick the top one.
        sentence_states.append(word_encoder_state_doc[-1])
      else: # Single layer: it's just a state vector.
        sentence_states.append(word_encoder_state_doc)

  sentence_encoder_initial_state = encoder_state_question

  print("[amnModel.amn] make sentence encoder_fw.")
  with variable_scope.variable_scope("sentence_encoder_fw",
                                     dtype=dtype) as scope:
    dtype = scope.dtype
    
    encoder_outputs_doc, encoder_state_doc = tf.nn.dynamic_rnn(
      cell, tf.stack(sentence_states, axis=1),
      initial_state=sentence_encoder_initial_state,
      dtype=dtype, swap_memory=True)

  print("[amnModel.amn] make sentence encoder_bw.")
  with variable_scope.variable_scope("sentence_encoder_bw",
                                     dtype=dtype) as scope:
    dtype = scope.dtype
  
    encoder_outputs_doc_bw, encoder_state_doc_bw = tf.nn.dynamic_rnn(
      cell, tf.stack(sentence_states[::-1], axis=1),
      initial_state=sentence_encoder_initial_state, dtype=dtype,
      swap_memory=True)
  
  # Reverse again, so the word which was originally the first word is at the
  # start again.
  encoder_outputs_doc_bw = tf.reverse(encoder_outputs_doc_bw,
                                      [False, True, False])
  
  # The outputs are the sums of states for both directions.
  # So state_i = forward_state_i + backward state_i.
  encoder_outputs_doc = tf.add(
    encoder_outputs_doc, encoder_outputs_doc_bw, name='combined_state')

  if type(encoder_state_doc) == type(tuple()):
    encoder_state_doc = tuple([x+y for x, y in zip(encoder_state_doc,
                                                   encoder_state_doc_bw)])
  else:
    encoder_state_doc += encoder_state_doc_bw

  print("[amnModel.amn] make memories.")

  # A list of 2D Tensors [batch_size x input_size].
  # NOTE that the number of items in this list determines the number of
  # decoding steps (i.e. the number of memories).
  if type(encoder_state_question) == type(tuple()):
    # If it's a multi-layered cell, encoder_state_question is a tuple of
    # states. We pick the top one
    memory_inputs = [encoder_state_question[-1]] * num_memories
  else: # Single layer: it's just a state vector
    memory_inputs = [encoder_state_question] * num_memories

  memory_outputs = []
  # Just a single RNN that unrolls for as many time steps as there are
  # memory_inputs
  with variable_scope.variable_scope("memories", dtype=dtype) as scope:
    # Here, we do not take the output of the previous step as input to the
    # next step. We just keep on feeding in the question representation..
    memory_outputs, memory_state, aSoftmaxes = seq2seq.attention_decoder(
      memory_inputs,
      encoder_state_doc, # Initial state.
      encoder_outputs_doc, # States to attend over.,
      cell,
      num_heads=num_heads,
      loop_function=None,
      bReturnSoftmaxes=True)

  # The states to attend over for the document encoding, i.e., the memories.
  attention_states = tf.stack(memory_outputs, axis=1)

  print("[amnModel.amn] make decoder.")
  with variable_scope.variable_scope("decoder", dtype=dtype) as scope:
    # Decoder.
    output_size = None
    if output_projection is None:
      cell = rnn_cell.OutputProjectionWrapper(cell, num_decoder_symbols)
      output_size = num_decoder_symbols

    if isinstance(feed_previous, bool):
      return aSoftmaxes, seq2seq.myEmbeddingAttentionDecoder(
        decoder_inputs,
        memory_state, # Initial state
        attention_states,
        cell,
        num_decoder_symbols,
        embeddings,
        num_heads=num_heads,
        output_size=output_size,
        output_projection=output_projection,
        feed_previous=feed_previous,
        initial_state_attention=initial_state_attention)

    # If feed_previous is a Tensor, we construct 2 graphs and use cond.
    def decoder(feed_previous_bool):
      reuse = None if feed_previous_bool else True
      with variable_scope.variable_scope(
          variable_scope.get_variable_scope(), reuse=reuse) as scope:
        outputs, state = seq2seq.myEmbeddingAttentionDecoder(
          decoder_inputs,
          memory_state,
          attention_states,
          cell,
          num_decoder_symbols,
          embeddings,
          num_heads=num_heads,
          output_size=output_size,
          output_projection=output_projection,
          feed_previous=feed_previous_bool,
          update_embedding_for_previous=False,
          initial_state_attention=initial_state_attention)
        state_list = [state]
        if nest.is_sequence(state):
          state_list = nest.flatten(state)
        return outputs + state_list

    outputs_and_state = control_flow_ops.cond(feed_previous,
                                              lambda: decoder(True),
                                              lambda: decoder(False))
    outputs_len = len(decoder_inputs)  # Outputs length same as decoder inputs.
    state_list = outputs_and_state[outputs_len:]
    state = state_list[0]
    if nest.is_sequence(encoder_state):
      state = nest.pack_sequence_as(structure=encoder_state,
                                    flat_sequence=state_list)
    return outputs_and_state[:outputs_len], state
